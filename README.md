# Texto

A minimal theme for [Lektor](http://getlektor.com/).

Usercase: to make it easier to publish some words (= a text). Write in markdown
or make use of Lektor's ["what you see is what you get"
editor](https://en.wikipedia.org/wiki/WYSIWYG).

**Screenshots** under [images/](https://gitlab.com/lsrdg/lektor-theme-texto/tree/master/images).

Theme built with [Responsivebp](https://responsivebp.com/).

**status**: the theme is being actively developed, see the
[issues](https://gitlab.com/lsrdg/lektor-theme-texto/issues) and
[milestones](https://gitlab.com/lsrdg/lektor-theme-texto/milestones).

## Text data modelling

Lektor has a powerfull [Data
Modelling](https://www.getlektor.com/docs/models/) ability. The idea behind
Texto is to make use this power in order to provide a good structure to text
driven content, despite of what kind of text the user need.

- `content/blog/` is a basic blog (`_model: blog`, ordered by date), blog posts
  inside it use `_model: blog-post`,
- `content/texts/` has `_model: texts` and accepts sub-directories with `_model:
  text-series` inside it, each series accepts as
  many `_model: text`'s as need,
- Anything else inside `content/` that is neither `/blog/` nor `/texts/` can
  have `_model: page`.


## Example-site

There is a fully functional example site you can try out
[here](https://gitlab.com/lsrdg/lektor-theme-texto/tree/master/example-site).

If you don't have Lektor installed, go ahead and [do
it](https://www.getlektor.com/docs/installation/). If Lektor is already
installed on your system, just `$ cd` onto the `/example-site` directory linked
above, and run `$ lektor server` to take a look at the example site.


## Configuration

Take a look at Lektor's documentation on [how to install a
theme](https://www.getlektor.com/docs/themes/installing/).

Keep in mind that theme support is still under development, that's why the
process is still achieved manually.

### If you don't have a website yet

Read the documentation about the [quickstart
command](https://www.getlektor.com/docs/quickstart/).

1. Run `$ lektor quickstart` and answer to the prompts.

   Choose a name for your project, choose a path for your project's root, say
   **No** to add a basic blog, enter your information (name, e-mail) and create
   the project.

   Move to inside your project:

   ```
   $ cd YOUR_PROJECT_ROOT/
   ```

2. Back to [installing a theme](https://www.getlektor.com/docs/themes/installing/), 
make sure you are on your project's root and make a `/themes` directory:
  
   ```
   $ pwd
   /home/YOUR_USERNAME/YOUR_PROJECT_ROOT/
   $ mkdir themes
   $ cd themes
   ```

3. Git clone *Lektor-theme-texto*:

   ```
   $ pwd
   /home/YOUR_USERNAME/YOUR_PROJECT_ROOT/themes
   $ git clone https://gitlab.com/lsrdg/lektor-theme-texto.git
   ```

4. Back to your project's root, delete what was created by `$ lektor
quickstart`:

   ```
   $ cd ../
   $ pwd
   /home/YOUR_USERNAME/YOUR_PROJECT_ROOT/
   $ rm -r {assets,content,models,templates}
   ```

5. In order to get a basic website working locally (which can be edited later),
you need to copy or move some files and folders from the `/example-site` inside
`/themes`. If you want to copy (and keep the `example-site` for future
reference:

   ```
   $ pwd
   /home/YOUR_USERNAME/YOUR_PROJECT_ROOT/
   $ cp -r themes/lektor-theme-texto/example-site/{content,databags}
   ```

6. You're done. Run `$ lektor server` and check it out.
   Additionally, you could add the theme to your projects file, [as
   documented](https://www.getlektor.com/docs/themes/installing/). It is not
   required, but it will probably avoid unknown issues with future releases of
   Lektor:

   ```
   # /home/YOUR_USERNAME/YOUR_PROJECT_ROOT/YOUR_PROJECT_NAME.lektorproject
   themes = lektor-theme-texto
   ```

# Sass

In theory, Lektor makes use of
[Webpack](https://www.getlektor.com/docs/guides/webpack/) to handle Sass. At
some, Lektor-theme-texto will support Webpack (see [issue
#11](https://gitlab.com/lsrdg/lektor-theme-texto/issues/11), and it means that
by running `$ lektor server -f webpack`, the server will run and all `.scss`
files will be compiled to `.css`.

Not yet. However, since #10, the theme comes with a `scss/` folder. Sass can be
trigged manually:

```
$ pwd
/lektor-theme-texto/
$ sass scss/texto.scss themes/lektor-theme-texto/assets/static/gen/styles.scss
$ lektor server
```

## License

Texto is licensed under the [BSD-3-Clause license](LICENSE.md).
